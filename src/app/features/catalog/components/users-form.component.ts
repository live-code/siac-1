import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {User} from "../../../model/user";

@Component({
  selector: 'app-users-form',
  template: `
    <input
      type="text"
      [formControl]="input"
    >

    <button
      [disabled]="input.invalid"
      (click)="saveHandler()"
    >  {{activeUser.id ? 'EDIT' : 'ADD'}} </button>
  `,
})
export class UsersFormComponent {
  @Input() activeUser: Partial<User> = {} ;
  @Output() save = new EventEmitter<Partial<User>>();

  input = new FormControl('', {
    nonNullable: true,
    validators: [ Validators.required, Validators.minLength(3)]
  })

  saveHandler() {
    this.save.emit({
      name: this.input.value,
    })
  }

  ngOnChanges() {
    if (this.activeUser?.id) {
      this.input.setValue(this.activeUser.name!)
    } else {
      this.input.reset()
    }
  }
}

