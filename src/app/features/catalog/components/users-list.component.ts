import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list',
  template: `

    <ul class="list-group">
      <app-users-list-item
        *ngFor="let user of users"
        [user]="user"
        [selected]="user.id === activeUser.id"
        (itemClick)="itemClick.emit($event)"
        (itemDelete)="itemDelete.emit($event)"
      ></app-users-list-item>
    </ul>

  `,
})
export class UsersListComponent {
  @Input() users: User[] = [];
  @Input() activeUser: Partial<User> = {} ;
  @Output() itemClick = new EventEmitter<User>()
  @Output() itemDelete = new EventEmitter<number>()
}
