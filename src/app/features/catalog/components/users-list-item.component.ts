import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import {User} from "../../../model/user";

@Component({
  selector: 'app-users-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      [ngClass]="{'bg-warning': selected}"
      (click)="itemClick.emit(user)"
    >
      <div class="d-flex justify-content-between align-items-center">
        <div>{{user.name}}</div>

        <div class="d-flex gap-2">
          <i 
            class="fa fa-arrow-circle-o-right"
            [routerLink]="'/catalog/' + user.id"
          ></i>
          <i class="fa fa-trash"
             (click)="deleteHandler(user, $event)"></i>
          <i class="fa fa-arrow-circle-down"
             (click)="isOpen = !isOpen"></i>
        </div>
      </div>
      <div *ngIf="isOpen">
        <app-static-map [city]="user.city" [zoom]="zoomValue" />
        <button (click)="zoomValue = zoomValue - 1">-</button>
        <button (click)="zoomValue = zoomValue + 1">+</button>
      </div>
    </li>
  `,
})
export class UsersListItemComponent {
  @Input() user!: User;
  @Input() selected: boolean = false;
  @Output() itemClick = new EventEmitter<User>()
  @Output() itemDelete = new EventEmitter<number>()

  isOpen = false;
  zoomValue = 7

  deleteHandler(user: User, event: MouseEvent) {
    event.stopPropagation();
    this.itemDelete.emit(user.id)
  }
}
