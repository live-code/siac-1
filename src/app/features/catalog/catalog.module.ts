import {NgModule} from "@angular/core";
import {CatalogComponent} from "./catalog.component";
import {UsersListComponent} from "./components/users-list.component";
import {UsersFormComponent} from "./components/users-form.component";
import {UsersListItemComponent} from "./components/users-list-item.component";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import { CatalogSalesComponent } from './pages/catalog-sales.component';
import { CatalogListComponent } from './pages/catalog-list.component';
import {UsersService} from "./services/users.service";

@NgModule({
  declarations: [
    CatalogComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersListItemComponent,
    CatalogSalesComponent,
    CatalogListComponent,
  ],
  providers: [
    UsersService
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([

      {
        path: '',
        component: CatalogComponent,
        children: [
          { path: 'list', component: CatalogListComponent},
          { path: 'sales', component: CatalogSalesComponent},
          { path: '', redirectTo: 'list', pathMatch: 'full'}
        ]
      },
    ])
  ]
})
export class CatalogModule {}
