import { ChangeDetectorRef, Injectable } from '@angular/core';
import {User} from "../../../model/user";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class UsersService {
  users: User[] = [];
  activeUser: Partial<User> = {} ;
  error: string | null = null;

  constructor(
    private http: HttpClient,
  ) {}

  getUsers() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => {
        this.users = res;
      })
  }


  saveHandler(formData: Partial<User>) {
    if (this.activeUser?.id) {
      this.editHandler(formData);
    } else {
      this.addHandler(formData)
    }
  }

  editHandler(formData: Partial<User>) {
    this.error = null;
    this.http.patch<User>(`http://localhost:3000/users/${this.activeUser?.id}`, formData)
      .subscribe(res => {
        /*const index = this.users.findIndex(item => item.id === this.activeUser?.id);
        this.users[index] = res;*/

        this.users = this.users.map(u => {
          return u.id === this.activeUser?.id ? res : u
        })
      })
  }

  addHandler(formData: Partial<User>) {
    this.error = null;
    this.http.post<User>('http://localhost:3000/users', formData)
      .subscribe(
        res => {
          this.users = [...this.users, res]
          this.activeUser = {};
        },
        err => {

        }
      )
  }

  deleteHandler(id: number) {
    this.error = null;
    this.http.delete(`http://localhost:3000/users/${id}`)
      .subscribe({
        next: () => {
          this.users = this.users.filter(u => u.id !== id)

          if(id === this.activeUser?.id) {
            this.activeUser = {};
          }

        },
        error: (err) => {
          this.error = 'Delete failed';
        }
      })
  }

  selectUserHandler(user: User) {
    this.activeUser = user;
  }
}


