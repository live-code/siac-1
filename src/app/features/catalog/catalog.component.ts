import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <button routerLink="list" routerLinkActive="bg-warning">List</button>
    <button routerLink="/catalog/sales" routerLinkActive="bg-warning">Offerte</button>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class CatalogComponent {


}
