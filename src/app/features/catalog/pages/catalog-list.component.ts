import { ChangeDetectionStrategy, Component } from '@angular/core';
import {UsersService} from "../services/users.service";

@Component({
  selector: 'app-catalog-list',
  template: `
    <app-error-msg
      *ngIf="usersService.error"
      [msg]="usersService.error"
    />

    <app-users-form
      [activeUser]="usersService.activeUser"
      (save)="usersService.saveHandler($event)"
    />

    <app-users-list
      [users]="usersService.users"
      [activeUser]="usersService.activeUser"
      (itemClick)="usersService.selectUserHandler($event)"
      (itemDelete)="usersService.deleteHandler($event)"
    />
  `,
  styles: [
  ]
})
export class CatalogListComponent {

  constructor(public usersService: UsersService) {}

  ngOnInit() {
    this.usersService.getUsers()
  }
}
