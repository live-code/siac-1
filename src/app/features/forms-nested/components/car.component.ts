import { Component } from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';

@Component({
  selector: 'app-car',
  template: `
   <h1>
      <span *ngIf="form.valid">✅</span>
      Car
    </h1>

    <div [formGroup]="form">
      <app-my-input
        (blur)="onTouch()"
        formControlName="brand"
      ></app-my-input>
      
      <input 
        (blur)="onTouch()" 
        type="text" formControlName="name" class="form-control">
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, useExisting: CarComponent, multi: true
    }
  ],
})
export class CarComponent implements ControlValueAccessor{
  form = this.fb.group({
    brand: ['A', Validators.required],
    name: 'B',
  })
  onTouch!: () => void;

  constructor(private fb: FormBuilder) {
  }
  registerOnChange(fn: any): void {
    this.form.valueChanges
      .subscribe(obj => {
        fn(obj)
      })
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(obj: any): void {
    console.log(obj)
    this.form.patchValue(obj, { emitEvent: false })
  }

}
