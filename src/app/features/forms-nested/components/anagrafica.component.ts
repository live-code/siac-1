import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-anagrafica',
  template: `
    <div [formGroup]="form">
      <ng-container formGroupName="user">
        <h1>
          <span *ngIf="form.get('user')?.valid">✅</span>
          User
        </h1>
        <i
          *ngIf="form.get('user.name')?.pending"
          class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
  
        <input
          type="text"
          formControlName="name"
          class="form-control"
          placeholder="Name"
          [ngClass]="{'is-invalid': form.get('user.name')?.invalid && form.dirty}"
        >
        <input
          type="text" formControlName="surname"
          class="form-control"
          placeholder="Surname"
          [ngClass]="{'is-invalid': form.get('user.surname')?.invalid && form.dirty}"
        >
      </ng-container>
    </div>
  `,
})
export class AnagraficaComponent {
  @Input() form!: FormGroup;
}
