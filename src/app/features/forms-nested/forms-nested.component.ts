import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { delay } from 'rxjs';
import { User } from '../../model/user';
import { UserValidatorService } from '../../shared/forms/user-validator.service';

@Component({
  selector: 'app-forms-nested',
  template: `
   
    {{form.pending}}
    <form [formGroup]="form">
      <input type="text" formControlName="orderNumber" class="form-control" >
      
      <ng-container formGroupName="user">
        <h1>
          <span *ngIf="form.get('user')?.valid">✅</span>
          User
        </h1>
        <i
          *ngIf="form.get('user.name')?.pending"
          class="fa fa-spinner fa-spin fa-3x fa-fw"></i>

        <input 
          type="text" 
          formControlName="name"
          class="form-control"
          [ngClass]="{'is-invalid': form.get('user.name')?.invalid && form.dirty}"
        >
        <input
          type="text" formControlName="surname"
          class="form-control"
          [ngClass]="{'is-invalid': form.get('user.surname')?.invalid && form.dirty}"
        >
      </ng-container>
      
      <h1>
        <span *ngIf="form.get('car')?.valid">✅</span>
        Car
      </h1>
      
      <div formGroupName="car">
        <input type="text" formControlName="brand" class="form-control">
        <input type="text" formControlName="name" class="form-control">
      </div>
      
      <div *ngIf="form.get('passwords')?.errors?.['passNoMatch']">Pass does not match</div>
      
      
      <div formGroupName="passwords">
        <!--PASS 1-->
        <input 
          type="text" placeholder="pass 1" formControlName="password1"
          class="form-control"
          [ngClass]="{'is-invalid': form.get('passwords.password1')?.invalid}"
        >
        <!--PASS 2-->
        <input
          type="text" placeholder="pass 2" formControlName="password2"
          class="form-control"
          [ngClass]="{'is-invalid': form.get('passwords.password2')?.invalid}"
        >
      </div>
      
      <button [disabled]="form.invalid || form.pending">SAVE</button>
    </form>
    <pre>{{form.value | json}}</pre>
  `,
})
export class FormsNestedComponent {
  form = this.fb.group({
    orderNumber: '',
    user: this.fb.group({
      name: ['', [Validators.required], this.userValidator.checkUserName()],
      surname: ['', Validators.required],
    }),
    car: this.fb.group({
      brand: ['', Validators.required],
      name: ['', Validators.required],
    }),
    passwords: this.fb.group(
      {
        password1: ['', [Validators.required, Validators.minLength(3)]],
        password2: ['', [Validators.required, Validators.minLength(3)]],
      },
      {
        validators: passwordMatch()
      }
    )
  })

  constructor(private fb: FormBuilder, private userValidator: UserValidatorService) {

    const res = {
      orderNumber: '123',
      user: {
        name: 'Fabio',
        surname: 'biondi'
      },
      car: {
        brand: 'Fiat',
        name: '127'
      },

    }
    // this.form.patchValue(res)
  }
}


export function passwordMatch() {
  return (group: FormGroup) => {
    const p1 = group.get('password1');
    const p2 = group.get('password2');
    if(p1?.value !==  p2?.value) {
      p2?.setErrors({ passWrong: true})
      return { passNoMatch: true }
    }

    p2?.setErrors(null)
    return null;
  };
}




