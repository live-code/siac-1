import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FormsNestedComponentCVA } from './forms-nested-cva.component';
import { FormsNestedComponent } from './forms-nested.component';
import { AnagraficaComponent } from './components/anagrafica.component';
import { CarComponent } from './components/car.component';


const routes: Routes = [
  { path: '', component: FormsNestedComponent },
  { path: 'cva', component: FormsNestedComponentCVA },
];

@NgModule({
  declarations: [
    FormsNestedComponent,
    FormsNestedComponentCVA,
    AnagraficaComponent,
    CarComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class FormsNestedModule { }
