import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { UserValidatorService } from '../../shared/forms/user-validator.service';

@Component({
  selector: 'app-forms-nested-cva',
  template: `
   
    <h1>Form Nested with CVA</h1>
    <form [formGroup]="form">
      <input type="text" formControlName="orderNumber" class="form-control"  placeholder="order number">
      <app-anagrafica [form]="form"></app-anagrafica>
      <app-car formControlName="car"></app-car>
      
      <button [disabled]="form.invalid || form.pending">SAVE</button>
      
    </form>
    <pre>{{form.value | json}}</pre>
  `,
})
export class FormsNestedComponentCVA {
  form = this.fb.group({
    orderNumber: '',
    user: this.fb.group({
      name: ['', [Validators.required], this.userValidator.checkUserName()],
      surname: ['', Validators.required],
    }),
    car: {
      brand: 'FIAT',
      name: '127'
    },
  })

  constructor(private fb: FormBuilder, private userValidator: UserValidatorService) {

    const res = {
      orderNumber: '123',
      user: {
        name: 'Fabio',
        surname: 'biondi'
      },
      car: {
        brand: 'Audi',
        name: 'xyz'
      },

    }

    setTimeout(() => {
      this.form.patchValue(res)
    }, 2000)
  }
}


export function passwordMatch() {
  return (group: FormGroup) => {
    const p1 = group.get('password1');
    const p2 = group.get('password2');
    if(p1?.value !==  p2?.value) {
      p2?.setErrors({ passWrong: true})
      return { passNoMatch: true }
    }

    p2?.setErrors(null)
    return null;
  };
}




