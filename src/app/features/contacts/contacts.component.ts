import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';

export const ALPHA_NUMERIC_REGEX = /^\w+$/

@Component({
  selector: 'app-contacts',
  template: `
    <form (submit)="send()" [formGroup]="form">
      <input type="checkbox" formControlName="isCompany"> Are you a company?

      <input
        type="text" placeholder="name" formControlName="name"
        class="form-control"
        [ngClass]="{'is-invalid': form.get('name')?.invalid && form.dirty}"
      >
      
      <div *ngIf="form.get('PICodFisc')?.errors as err">
        <div *ngIf="err['required']">required</div>
        <div *ngIf="err['alphaNumeric']">no symbols allowed</div>
        <div *ngIf="err['vat']">Partita iva invalida</div>
        <div *ngIf="err['cf']">cf  invalido</div>
      </div>
    
      <input
        type="text" 
        [placeholder]="form.get('isCompany')?.value ? 'PI' : 'CodFisc'" 
        formControlName="PICodFisc"
        class="form-control"
        [ngClass]="{'is-invalid': form.get('PICodFisc')?.invalid && form.dirty}"
      >
      <button [disabled]="form.invalid">INVIA</button>
    </form>

    <pre>{{form.value | json}}</pre>
  `,
  styles: [
  ]
})
export class ContactsComponent {
  form = this.fb.nonNullable.group(
    {
      name: ['', [Validators.required, Validators.minLength(3)] ],
      PICodFisc: [''],
      isCompany: false
    },
    {
    //   updateOn: 'submit'
    }
  )

  constructor(private fb: FormBuilder) {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if(isCompany) {
          // company
          this.form.get('PICodFisc')?.setValidators([
            Validators.required, lengthValidator(11, 'vat')
          ])
        } else {
          // user
          this.form.get('PICodFisc')?.setValidators([Validators.required, lengthValidator(16, 'cf')])
        }
        this.form.get('PICodFisc')?.updateValueAndValidity()

      })

    this.form.patchValue({ isCompany: false })
  }

  send() {
    console.log(this.form.value)
    // console.log(this.form.getRawValue())
  }
}
/*


export function lengthValidator(requiredLength: number, errorType: string): ValidatorFn {
  return (c) => {
    return c.value && c.value.length === requiredLength ?
      null :
      { [errorType]: true };
  }
}
*/



export const lengthValidator = (requiredLength: number, errorType: string): ValidatorFn =>
  (c) =>
    c.value && c.value.length === requiredLength ?
      null :
      { [errorType]: true };




export function alphaNumericValidator(c: FormControl) {
  return c.value && !c.value.match(ALPHA_NUMERIC_REGEX) ?
    { alphaNumeric: true } :
    null;
}
