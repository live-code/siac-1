import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CatalogDetailsComponent } from './catalog-details.component';


const routes: Routes = [
  { path: '', component: CatalogDetailsComponent }
];

@NgModule({
  declarations: [
    CatalogDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CatalogDetailsModule { }
