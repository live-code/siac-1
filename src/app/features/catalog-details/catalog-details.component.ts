import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounce, debounceTime, filter, map } from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'app-catalog-details',
  template: `
    <div *ngIf="msg" class="alert alert-success">
      {{msg}}
      <i class="fa fa-times" (click)="msg = null"></i>
    </div>

    <!-- <div *ngIf="form.invalid">Il form è invalido</div>-->

    <form [formGroup]="form" (submit)="save()">

      <div *ngIf="form.get('name')?.errors as err">
        <div *ngIf="err['required']">Campo Obbligatorio</div>
        
        <div *ngIf="err['minlength']">
          Minimo {{err['minlength']?.['requiredLength']}} chars. 
        </div>
      </div>
      
      <input
        type="text" formControlName="name"
        class="form-control"
        [ngClass]="{
          'is-invalid': form.get('name')?.invalid,
          'is-valid': form.get('name')?.valid
        }"
      >
      
      <app-input-progress-bar
        [error]="form.get('name')?.errors!"
      ></app-input-progress-bar>
      
      

      <br>
      
      <input
        type="text" formControlName="city"
        class="form-control"
        [ngClass]="{
          'is-invalid': form.get('city')?.invalid,
          'is-valid': form.get('city')?.valid
        }"
      >
      
      <!--<div class="progress" role="progressbar">
        <div class="progress-bar" [style.width.%]="getPerc('city')"></div>
      </div>-->
      
      <hr>
      <button [disabled]="form.invalid">SAVE</button>
    </form>

    <button (click)="doSomething()">XYZ</button>


    <button (click)="resetForm()">reset</button>
    <pre>value: {{form.value | json}}</pre>
    <pre>valid: {{form.valid | json}}</pre>
    <pre>dirty: {{form.dirty | json}}</pre>
    <pre>touched: {{form.touched | json}}</pre>
  `,
  styles: [``]
})
export class CatalogDetailsComponent {
  value = 25;

  form = new FormGroup({
    name: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required, Validators.minLength(5)]
    }),
    city: new FormControl('', { validators: [Validators.required, Validators.minLength(3)]})
  })

  data: User | undefined;
  msg: string | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {

    activatedRoute.params
      .subscribe(params => {
          this.getUser(params['id'])
      })
  }

  getUser(id: string) {
    this.http.get<User>('http://localhost:3000/users/' + id)
      .subscribe(res => {
        this.data = res;
        this.form.patchValue(res)
      })
  }


  gotoNext() {
    this.router.navigateByUrl('/catalog/5')
  }

  resetForm() {
    this.form.reset();
  }

  save() {
    this.http.patch<User>('http://localhost:3000/users/' + this.data?.id, this.form.value)
      .subscribe(res => {
        /*this.data = res;
        this.form.patchValue(res)*/
        this.msg = 'dati salvati'
      })
  }

  doSomething() {

  }

  showMsg() {
    return !!this.msg
  }
}
