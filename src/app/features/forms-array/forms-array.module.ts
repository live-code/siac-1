import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FormsArrayComponent } from './forms-array.component';


const routes: Routes = [
  { path: '', component: FormsArrayComponent }
];

@NgModule({
  declarations: [
    FormsArrayComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedModule
  ]
})
export class FormsArrayModule { }
