import { Component } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-forms-array',
  template: `
    <form [formGroup]="form">
      <app-my-input 
        formControlName="name"
        label="Your Super Name"
        [alphaNumeric]="true"
      ></app-my-input>

      <hr>

      <h1>Items array</h1>
      <div formArrayName="items">
        <div
          *ngFor="let item of items.controls; let i = index; let last = last"
          [formGroupName]="i"
        >
          <input type="text" placeholder="name" formControlName="name">
          <input type="text" placeholder="price" formControlName="price">
          <i class="fa fa-trash" (click)="removeItem(i)" *ngIf="items.controls.length > 1"></i>
          <i class="fa fa-plus-circle" (click)="addItem()" *ngIf="item.valid && last "></i>
        </div>
      </div>

      <h1>Custom Form Controls</h1>
      
      <app-color-picker formControlName="bg"/>
      <app-color-picker formControlName="color" [colors]="['red', 'pink', 'purple']"/>
      <app-rate formControlName="rate"></app-rate>

      <br>
      <button [disabled]="form.invalid">SAVE</button>
    </form>
    

    <pre> {{form.value | json}}</pre>
    <pre> {{form.dirty | json}}</pre>
    <pre> {{form.touched | json}}</pre>
  `,
})
export class FormsArrayComponent {

  form = this.fb.group({
    name: ['', [
      Validators.required,
      Validators.minLength(5),
    ]],
    surname: ['', [Validators.required]],
    bg: ['', [Validators.required, Validators.minLength(3)]],
    color: 'red',
    rate: 2,
    items: this.fb.array([])
  })

  items = this.form.get('items') as FormArray;

  constructor(private fb: FormBuilder) {
    this.addItem();

    setTimeout(() => {
      /*this.form.patchValue({
        name: 'pippo',
        color: 'purple',
        rate: 4
      })*/
    }, 2000)
  }

  addItem() {
    this.items.push(
      this.fb.group({
        name: ['', Validators.required],
        price: [0, Validators.required]
      })
    )
  }

  removeItem(i: number) {
    this.items.removeAt(i)
  }
}
