import { Component } from '@angular/core';
import {CardComponent} from "../../shared/components/card.component";
import {UsersService} from "../catalog/services/users.service";
import {ThemeService} from "../../core/services/theme.service";

@Component({
  selector: 'app-uikit',
  template: `

    <h1>Current theme is {{themeService.theme}}</h1>
    <button (click)="themeService.theme = 'light'">light</button>
    <button (click)="themeService.theme = 'dark'">dark</button>

    <app-card
      title="ciao"
      icon="fa fa-eye"
      (iconClick)="visible = true"
    >
      <input type="text">
      <input type="text">
      <input type="text">
    </app-card>

    <app-card
      *ngIf="visible"
      title="Profile"
      variant="success"
      icon="fa fa-facebook"
    >
      lorem
    </app-card>

    <app-static-map city="Gorizia" />
  `,

})
export class UikitComponent {
  visible = false;

  constructor(public themeService: ThemeService) {}


  openUrl() {
    window.open('http://www.google.com')
  }
}
