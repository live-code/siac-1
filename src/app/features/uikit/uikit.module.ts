import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UikitComponent} from "./uikit.component";
import {SharedModule} from "../../shared/shared.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    UikitComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: UikitComponent}
    ])
  ]
})
export class UikitModule { }
