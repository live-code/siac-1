import { Injectable } from '@angular/core';


type Theme = 'dark' | 'light'

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private value: Theme = 'dark'

  constructor() {
    const theme = localStorage.getItem('theme') as Theme;
    if (theme) {
      this.value = theme
    }

  }

  set theme(theme: Theme) {
    localStorage.setItem('theme', theme)
    this.value = theme;
  }

  get theme() {
    return this.value;
  }

}
