import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';
import {ThemeService} from "../services/theme.service";

@Component({
  selector: 'app-navbar',
  template: `
    <div
      class="p-3"
      [ngClass]="{
        'bg-dark text-white': themeService.theme === 'dark',
        'bg-info': themeService.theme === 'light'
      }"
    >
      Theme: {{themeService.theme}}
      <button [routerLink]="'home'" routerLinkActive="bg-warning">home</button>
      <button routerLink="catalog" routerLinkActive="bg-warning">catalog</button>
      <button routerLink="uikit" routerLinkActive="bg-warning">uikit</button>
      <button routerLink="contacts" routerLinkActive="bg-warning">contacts</button>
      <button routerLink="forms-array" routerLinkActive="bg-warning">array + CVA</button>
      <button routerLink="forms-nested" routerLinkActive="bg-warning" [routerLinkActiveOptions]="{ exact: true }">forms-nested</button>
      <button routerLink="forms-nested/cva" routerLinkActive="bg-warning" >forms-nested CVA</button>
    </div>
  `,
})
export class NavbarComponent {
  constructor(public themeService: ThemeService) {}

}
