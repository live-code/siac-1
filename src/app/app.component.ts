import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { delay, forkJoin, interval, merge } from 'rxjs';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
     <hr>

    <div class="container">
     <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
}
