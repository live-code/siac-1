import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {ErrorMsgComponent} from "./components/error-msg.component";
import {StaticMapComponent} from "./components/static-map.component";
import {CardComponent} from "./components/card.component";
import { InputProgressBarComponent } from './components/input-progress-bar.component';
import { ColorPickerComponent } from './forms/color-picker.component';
import { RateComponent } from './forms/rate.component';
import { MyInputComponent } from './forms/my-input.component';



@NgModule({
  declarations: [
    ErrorMsgComponent,
    CardComponent,
    StaticMapComponent,
    InputProgressBarComponent,
    ColorPickerComponent,
    RateComponent,
    MyInputComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    ErrorMsgComponent,
    CardComponent,
    StaticMapComponent,
    InputProgressBarComponent,
    ColorPickerComponent,
    RateComponent,
    MyInputComponent,
  ]
})
export class SharedModule { }
