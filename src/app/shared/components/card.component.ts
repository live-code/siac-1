import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div
        class="card-header"
        [ngClass]="{
            'bg-success': variant === 'success',
            'bg-danger': variant === 'danger'
        }"
      >
        <div class="d-flex justify-content-between align-items-center">
          <div>{{title}}</div>
          <i
            (click)="iconClick.emit()"
            *ngIf="icon" [class]="icon"
          ></i>
        </div>
      </div>
      <div class="card-body">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input({ required: true }) title!: string;
  @Input() variant: 'success' | 'danger' | undefined;
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter();

}

