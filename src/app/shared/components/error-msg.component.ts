import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-error-msg',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      class="alert alert-danger"
      >{{msg}}</div>
    
  `,
})
export class ErrorMsgComponent {
  @Input() msg: string | null = null

}
