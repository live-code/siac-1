import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    <img
      width="100%"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + city + '&size=600,300&zoom=' + zoom" alt="">
  `,
})
export class StaticMapComponent {
  @Input({ required: true }) city: string = ''
  @Input() zoom = 10
}
