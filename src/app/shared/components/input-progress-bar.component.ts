import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-input-progress-bar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="progress" role="progressbar">
      <div class="progress-bar" 
           [style.width.%]="getPerc()"></div>
    </div>
  `,
})
export class InputProgressBarComponent {
  @Input() error: ValidationErrors | null = null;

  getPerc() {
    if (!this.error?.['minlength']) {
      return 0
    }
    const actualLength = this.error?.['minlength']?.['actualLength']
    const requiredLength = this.error?.['minlength']?.['requiredLength']
    return (actualLength / requiredLength) * 100;
  }

  render() {
    console.log('render perc', this.error)
  }
}
