import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-color-picker',
  template: `
    
    <div class="d-flex gap-2">
      <div
        *ngFor="let c of colors"
        class="cell"
        [ngClass]="{active: c === selectedColor}"
        [style.background-color]="c"
        (click)="onSelectColorHandler(c)"
      >
      </div>
    </div>
  `,
  styles: [`
    .cell {width: 30px;height: 30px;}
    .active { border: 2px solid black}
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, useExisting: ColorPickerComponent, multi: true
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor{
  @Input() colors = [
    '#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'
  ];
  selectedColor: string | undefined;
  onChange!: (color: string) => void
  onTouched!: () => void;

  onSelectColorHandler(c: string) {
    this.selectedColor = c;
    this.onChange(c)
    this.onTouched()
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: string): void {
    this.selectedColor = color;
  }

}
