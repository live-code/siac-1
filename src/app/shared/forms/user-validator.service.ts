import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { debounceTime, delay, first, map, mergeMap, of, timer } from 'rxjs';
import { User } from '../../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserValidatorService {

  constructor(private http: HttpClient) {
  }

  checkUserName() {
    return (c: AbstractControl) => {
      return timer(1000)
        .pipe(
          first(),
          mergeMap(() => this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)),
          map((res) => res.length ? { notAvailable: true } : null),

        )
    }
  }
}
