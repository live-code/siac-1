import { Component } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-rate',
  template: `
    
    <i
      class="fa"
      *ngFor="let star of [null, null,null,null,null]; let i = index"
      [ngClass]="{
        'fa-star': i <= value - 1,
        'fa-star-o': i > value - 1
      }"
      (click)="clickHandler(i+1)"
    ></i>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: RateComponent,
      multi: true
    }
  ]
})
export class RateComponent implements ControlValueAccessor{
  value: number = 0;
  onChange!: (rate: number) => void
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: number): void {
    this.value = value;
  }

  clickHandler(index: number) {
    this.value = index;
    this.onChange(index)
  }

}
