import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Injector, Input, Optional, Output, SkipSelf } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl, ValidationErrors, Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-my-input',
  template: `
    <div *ngIf="ngControl.errors as err">
      <div *ngIf="err['required'] && ngControl.control?.parent?.dirty">Field required</div>
      <div *ngIf="err['minlength'] && ngControl.control?.parent?.dirty">
        Min {{err['minlength']['requiredLength']}} chars
      </div>
      <div *ngIf="err['alphaNumeric'] && ngControl.control?.parent?.dirty">
        Field must contain letters and numbers...
      </div>
    </div>

    <div>
      <label>{{label}} </label>
      <input
        type="text"
        class="form-control"
        [formControl]="input"
        (blur)="onTouched(); blur.emit()"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALIDATORS, useExisting: MyInputComponent, multi: true },
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInputComponent, multi: true }
  ]
})
export class MyInputComponent implements ControlValueAccessor, Validator {
  @Input() label = ''
  @Input() alphaNumeric = false;
  @Input() required = true;
  @Output() blur = new EventEmitter()

  input = new FormControl();
  onTouched!: () => void;
  ngControl!: NgControl;

  constructor(private injector: Injector) {}

  ngOnInit() {
    this.ngControl = this.injector.get(NgControl)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: string): void {
    this.input.setValue(value)
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return c.value &&
           !c.value.match(ALPHA_NUMERIC_REGEX)  &&
           this.alphaNumeric ?
      { alphaNumeric: true } :
      null
  }

}
const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
