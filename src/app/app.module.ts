import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "./core/core.module";
import {UsersService} from "./features/catalog/services/users.service";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'catalog',
        loadChildren: () => import('./features/catalog/catalog.module')
          .then(file => file.CatalogModule)
      },
      { path: 'catalog/:id', loadChildren: () => import('./features/catalog-details/catalog-details.module').then(m => m.CatalogDetailsModule) },
      { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule)},
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
      { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
      { path: '', redirectTo: 'home', pathMatch:  'full'},
      { path: 'forms-nested', loadChildren: () => import('./features/forms-nested/forms-nested.module').then(m => m.FormsNestedModule) },
      { path: 'forms-array', loadChildren: () => import('./features/forms-array/forms-array.module').then(m => m.FormsArrayModule) },
      // { path: '**', component: }
    ])
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
